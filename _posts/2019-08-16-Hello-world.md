---
layout: post
title: Hello World
Description: >
  The Ruby Team Pages are up and running.
author: Daniel Leidert
tags:
  - misc
  - ruby-team
---

The [Debian Ruby pages are online][DRP]. This place can collect any information
about the Debian Ruby team and even offers blogging functionality.  It has been
built using [Gitlab Pages][GLP] and Jekyll. The source can be found on
[salsa.d.o][DRG].

[DRP]: https://ruby-team.pages.debian.net/ "Homepage of the Debian Ruby team"
[GLP]: https://salsa.debian.org/help/user/project/pages/index.md
[DRG]: https://salsa.debian.org/ruby-team/ruby-team.pages.debian.net "Project sources on salsa.debian.org"

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
