---
layout: post
author: Daniel Leidert
title: 'Ruby transition and packaging hints #1 - Adjusting Ruby version in commands'
date: '2021-11-08 12:56 +0100'
description: >-
 This is the start of a series of hints to help speed up the Ruby 3.0
 transition. The first part is about fixing the ruby version to run scripts
 during tests.
tags:
- ftbfs
- rbconfig
- ruby3.0
- packaging
---

This is the first part of a series of short posts about issues that came up
during the Ruby 3.0 transition and how to fix them. Hopefully more team members
will join in and add their input.

During the Ruby 3.0 transition there are essentially two different Ruby
versions with two different binaries available, `/usr/bin/ruby2.7` and
`/usr/bin/ruby3.0`, while `/usr/bin/ruby` points to the current default
version, which is Ruby 2.7.

In some cases the tests shipped by the source packages will use shell commands
to run scripts or Ruby code. It is imparative that in these cases the Ruby
executable is not invoked by `/usr/bin/ruby` or `ruby`, because this will point
to Ruby 2.7 only and fail if the tests are invoked with Ruby version 3.

The fix is to rely on `RbConfig.ruby` which will point to the absolute pathname
of the ruby command for the current Ruby environment, e.g.

```
cmd = "#{RbConfig.ruby} ..."
```

This issue appeard for example in [`ruby-byebug`][ruby-byebug] and
[`ruby-backports`][ruby-backports]{: data-proofer-ignore="yes"}.

[ruby-byebug]: https://salsa.debian.org/ruby-team/ruby-byebug/-/blob/debian/11.1.3-2/debian/patches/996145-fix-tests-with-ruby3.patch
[ruby-backports]: https://salsa.debian.org/ruby-team/ruby-backports/-/blob/debian/3.21.0-1/debian/patches/use-system-library-paths-when-running-under-autopkgtest#L82-83

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
